Source: printrun
Section: misc
Priority: optional
Maintainer: Debian 3-D Printing Packages <3dprinter-general@lists.alioth.debian.org>
Uploaders: Rock Storm <rockstorm@gmx.com>,
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-python,
               libpython3-dev,
               python3,
               python3-setuptools,
Standards-Version: 4.5.1
Homepage: http://www.pronterface.com
Vcs-Git: https://salsa.debian.org/3dprinting-team/printrun.git
Vcs-Browser: https://salsa.debian.org/3dprinting-team/printrun

Package: printrun-common
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Description: 3D printer host suite common files
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This package contains all common and architecture-independent files for all
 suite components.

Package: printcore
Architecture: any
Depends: printrun-common (= ${source:Version}),
         python3-dbus,
         python3-psutil,
         python3-serial,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
Breaks: printrun (<< 2)
Replaces: printrun (<< 2)
Description: 3D printer host core commands
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This package contains printcore, the G-Code sender.

Package: pronsole
Architecture: all
Depends: printcore (>= ${source:Version}),
         printrun-common (= ${source:Version}),
         python3-appdirs,
         ${misc:Depends},
         ${python3:Depends},
Recommends: slic3r,
Breaks: printrun (<< 2)
Replaces: printrun (<< 2)
Description: Command-line 3D printer host
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This package contains scripts and metadata for pronsole, a command-line 3D
 printer host.

Package: pronterface
Architecture: all
Depends: pronsole (= ${source:Version}),
         python3-cairosvg,
         python3-gi,
         python3-libxml2,
         python3-numpy,
         python3-pyglet,
         python3-wxgtk4.0,
         ${misc:Depends},
         ${python3:Depends},
Recommends: slic3r,
Breaks: printrun (<< 2)
Replaces: printrun (<< 2)
Description: Graphical 3D printer host
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This package contains scripts and metadata for pronterface, a graphical 3D
 printer host.

Package: plater
Architecture: all
Depends: printcore (>= ${source:Version}),
         printrun-common (= ${source:Version}),
         python3-numpy,
         python3-pyglet,
         python3-wxgtk4.0,
         ${misc:Depends},
         ${python3:Depends},
Breaks: printrun (<< 2)
Replaces: printrun (<< 2)
Description: Graphical tool to prepare 3D printing plates
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This package contains scripts and metadata for plater, a graphical tool to
 set-up STL files onto the printing plate.

Package: printrun
Architecture: all
Section: metapackages
Depends: plater,
         printcore,
         pronsole,
         pronterface,
         ${misc:Depends},
Description: 3D printer hosts suite
 Printrun is a full suite of host interfaces for 3D printers and CNC,
 consisting of:
  * printcore, a standalone non-interactive G-Code sender
  * pronsole, an interactive command-line host
  * pronterface, a graphical host software with the same functionality as
    pronsole
 .
 Together with Slic3r they form a powerful 3d printing toolchain.
 .
 This metapackage pulls all Printrun suite components.
